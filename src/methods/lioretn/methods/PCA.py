# -*- coding: utf-8 -*-
"""
License: MIT
@author: gaj
E-mail: anjing_guo@hnu.edu.cn
Paper References:
    [1] P. S. Chavez Jr. and A. W. Kwarteng, “Extracting spectral contrast in Landsat Thematic Mapper image data using selective principal component analysis,” 
        Photogrammetric Engineering and Remote Sensing, vol. 55, no. 3, pp. 339–348, March 1989.
    [2] G. Vivone, L. Alparone, J. Chanussot, M. Dalla Mura, A. Garzelli, G. Licciardi, R. Restaino, and L. Wald, “A Critical Comparison Among Pansharpening Algorithms”, 
        IEEE Transaction on Geoscience and Remote Sensing, 2014.
"""

import numpy as np
from sklearn.decomposition import PCA as princomp
from src.methods.lioretn.utils import upsample_interp23

def PCA(pan, ms):
    M, N, c = pan.shape
    m, n, C = ms.shape
    ratio = int(np.round(M/m))

    # Check that the format of pan and ms image are similar
    assert int(np.round(M/m)) == int(np.round(N/n))

    image_hr = pan
    
    # Upsample
    u_ms = upsample_interp23(ms, ratio)
    
    p = princomp(n_components=C)
    pca_ms = p.fit_transform(np.reshape(u_ms, (M*N, C)))
    
    pca_ms = np.reshape(pca_ms, (M, N, C))
    
    I = pca_ms[:, :, 0]
    
    image_hr = (image_hr - np.mean(image_hr))*np.std(I, ddof=1)/np.std(image_hr, ddof=1)+np.mean(I)
    
    pca_ms[:, :, 0] = image_hr[:, :, 0]
    
    I_PCA = p.inverse_transform(pca_ms)
    
    # Equalization
    I_PCA = I_PCA-np.mean(I_PCA, axis=(0, 1))+np.mean(u_ms)
    
    # Adjustment
    I_PCA[I_PCA<0]=0
    I_PCA[I_PCA>1]=1
    
    return np.float16(I_PCA)