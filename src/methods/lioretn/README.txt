Organisation :
lioretn
|_ methods : set of classical pansharpening methods comming from https://github.com/codegaj/py_pansharpening/tree/
master
|_ lioretn.pdf : report
|_ reconstruct.py : main file with function run_reconstruction, you can directly modify both parameters method and approach within the function according to what method and approach you want to apply (see the report)
|_ utils.py : useful functions for the implementation of the different pansharpening methods
|_ wavelengths.py : functions to work with wavelengths of the multispectral images we have, depending on the approach we chose
|_ other .pdf files : references (see the report)